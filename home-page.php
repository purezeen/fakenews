<?php /* Template Name: Homepage */ get_header(); ?>

<!-- Hero section  -->
<section class="hero-section top-background bottom-background">
   <div class="container section padding-bottom-0">
      <div class="hero">

         <!-- Get hero post ids  -->
         <?php $hero_post_id = array(); ?>

         <?php
         $args = array(
            'post_type' => 'post',
            'category_name' => 'blog, drustvo-i-kultura, ekonomija, kvazinauka, politika, svet, studije',
            'posts_per_page' => 1,
            'post_status' => 'publish',
            'order' => 'DESC'
         );
         ?>
         <?php $the_query = new WP_Query( $args ); ?>

         <?php  if ($the_query->have_posts()) : ?>
         <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

         <?php $post_id = get_the_ID(); ?>
         <?php $hero_post_id[] = $post_id; ?>

         <div  class="hero__emphasize">

            <?php 
               if (has_post_thumbnail()) {
                  $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'large');
                  $backgroundImg = $backgroundImg[0];
               }else {
                  $backgroundImg = "";
               }
            ?>

            <a href="<?php echo get_permalink(); ?>"class="hero__emphasize-img image-16-9 cover overlay d-block"
               style="background-image: url(<?php echo $backgroundImg; ?>);">
            </a>
            <div class="hero__emphasize-title">
               <h1><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h1>
            </div>
         </div>
         <?php endwhile; ?>
         <?php  endif; ?>

         <?php
         $args = array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            'post_status' => 'publish',
            'order' => 'DESC',
            'offset' => 1,
            'category_name' => 'blog, drustvo-i-kultura, ekonomija, kvazinauka, politika, svet, studije',
         );
         ?>

         <?php $the_query = new WP_Query( $args ); ?>
         <ul class="list-square list-square--style title-font hero__list">
            <?php  if ($the_query->have_posts()) : ?>

            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

            <?php $categories = get_the_category(); 
               $cat_slug = array();
              foreach($categories as $cat) {
                $cat_slug[] = $cat->slug;
              }
            ?>

            <?php 
            foreach($cat_slug as $slug) {
               switch ($slug) {
                  case "politika":
                     $list_square_color = "list-square-color--politics";
                     break;
                  case "drustvo-i-kultura":
                     $list_square_color = "list-square-color--society";
                     break;
                  case "blog":
                     $list_square_color = "list-square-color--blog";
                     break;
                     case "ekonomija":
                     $list_square_color = "list-square-color--economy";
                     break;
                     case "kvazinauka":
                     $list_square_color = "list-square-color--science";
                     break;
                     case "svet":
                     $list_square_color = "list-square-color--world";
                     break;
                  case "studije":
                     $list_square_color = "list-square-color--tragac";
                     break;
                     case "medjutim":
                        $list_square_color = "list-square-color--truth";
                        break;
                              
                  default:
                     $list_square_color = "list-square-color--politics";
                  } 
            }
            ?>

            <?php $post_id = get_the_ID(); ?>
            <?php $hero_post_id[] = $post_id; ?>

            <li class="<?php echo $list_square_color; ?>"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php endwhile; ?>
            <?php  endif; ?>
         </ul>
      </div>
   </div>
</section>

<!-- Cart politics society, ukratko i medjutim  -->
<section class="container container--indent section">
   <div class="columns">
      <div class="column is-6 ">

         <!-- Get politics cat  -->
         <?php $cat_name = 'Politika' ?>
         <?php $cat_slug = 'politika'; ?>
         <?php $cart_cat_class = "cart-cat--politics"; ?>
         <?php $mgb_large = "mgb-large"; ?>

         <?php include(locate_template('template-parts/cart-cat.php')); ?>
         <!-- End politics cat  -->

         <!-- Get Society cat  -->
         <?php $cat_name = 'Društvo i kultura' ?>
         <?php $cat_slug = 'drustvo-i-kultura'; ?>
         <?php $cart_cat_class = "cart-cat--society"; ?>
         <?php $mgb_large = ""; ?>

         <?php include(locate_template('template-parts/cart-cat.php')); ?>
         <!-- End Society cat  -->
      </div>
      <div class="column is-6">
         <div class="shadow brief--two-columns">
            <!-- Cart Ukratko i medjutim  -->
            <div class="brief">
               <h2 class="section-title brief_border"><a href="/category/ukratko">Ukratko</a></h2>
               <div class="brief-scroll-slider">

                  <?php
               $args = array(
                  'post_type' => 'post',
                  'category_name' => 'ukratko',
                  'posts_per_page' => 3,
                  'post_status' => 'publish'
               );
               ?>
                  <?php $the_query = new WP_Query( $args ); ?>

                  <?php  if ($the_query->have_posts()) : ?>
                  <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                  <article class="brief-box">
                     <h4 class="data"><?php echo get_the_date(); ?></h4>
                     <a href="<?php echo the_permalink(); ?>">
                        <h3><?php the_title(); ?></h3>
                     </a>
                     <p><?php echo get_the_excerpt();?></p>
                  </article>

                  <?php endwhile; ?>
                  <?php  endif; ?>
               </div>
            </div>
            <div class="brief">
               <h2 class="section-title truth_border"><a href="/category/medjutim">Međutim</a></h2>
               <div class="brief-scroll-slider">
                  <?php
               $args = array(
                  'post_type' => 'post',
                  'category_name' => 'medjutim',
                  'posts_per_page' => 3,
                  'post_status' => 'publish'
               );
               ?>
                  <?php $the_query = new WP_Query( $args ); ?>

                  <?php  if ($the_query->have_posts()) : ?>
                  <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                  <article class="brief-box">
                     <h4 class="data"><?php echo get_the_date(); ?>0</h4>
                     <p><?php echo wp_trim_words( get_the_content(), 40, '...<a href="/category/medjutim/#scroll-to-' . $post->ID . '">Više</a>' );?></p>
                  </article>

                  <?php endwhile; ?>
                  <?php  endif; ?>



               </div>
            </div>
         </div>

      </div>
   </div>
</section>

<!-- Blog slider  -->
<?php include(locate_template('template-parts/blog-slider.php')); ?>
<!-- End blog slider  -->

<!-- Cart sciency, cart economy, cart top list  -->
<section class="container container--indent section">
   <div class="columns">
      <div class="column">
         <div class="top-list shadow mgb-large">
            <div class="top-list__title background-main">
               <h3 class="section-title">TOP 15 - NAJČEŠĆI IZVORI MANIPULACIJA</h3>
            </div>
            <ul class="top-list__name top-list__name--second unstyle-list">
            <?php 
              $top_15_tags = get_terms(array(
                   'taxonomy' => 'post_tag',
                   'orderby' => 'count',
                   'order' => 'DESC',
                   'number' => '15'
               ));
               foreach ($top_15_tags as $top_tags) {
                  $home_url = home_url() . '/tag/' . $top_tags->slug;
                  echo '<li><a href="' . $home_url . '">' . $top_tags->name . '</a></li>';
               }
             ?>
            </ul>
         </div>
         <?php 
           $top_5_tags = get_terms(array(
                'taxonomy' => 'city_tag',
                'orderby' => 'count',
                'order' => 'DESC',
                'number' => '5'
            ));
           if ($top_5_tags) { ?>
            <div class="top-list shadow">
               <div class="top-list__title background-blue">
                  <h3 class="section-title">TOP 5 – NAJCEŠĆE LOKACIJE MANIPULACIJA</h3>
               </div>
                  <ul class="top-list__name top-list__name--first top-list--blue unstyle-list">
                  <?php
                  foreach ($top_5_tags as $top_tags) {
                     $home_url = home_url() . '/city_tag/' . $top_tags->slug;
                     echo '<li><a href="' . $home_url . '">' . $top_tags->name . '</a></li>';
                  }
                  ?>
                  </ul>
            </div>
         <?php
         } 
         ?>
      </div>
      <div class="column">
         <!-- Get economy cat  -->
         <?php $cat_name = 'Ekonomija' ?>
         <?php $cat_slug = 'ekonomija'; ?>
         <?php $cart_cat_class = "cart-cat--economy"; ?>
         <?php $mgb_large = "mgb-large"; ?>
         <?php include(locate_template('template-parts/cart-cat.php')); ?>
         <!-- End economy cat  -->

         <!-- Get science cat  -->
         <?php $cat_name = 'Nauka' ?>
         <?php $cat_slug = 'kvazinauka'; ?>
         <?php $cart_cat_class = "cart-cat--science"; ?>
         <?php $mgb_large = "mgb_large"; ?>
         <?php include(locate_template('template-parts/cart-cat.php')); ?>
         <!-- End science cat  -->
      </div>
   </div>
</section>

<!-- Banner 1 -->
<?php if ( have_rows( 'information_banner_with_internal_link' ) ) : ?>
   <?php while ( have_rows( 'information_banner_with_internal_link' ) ) : the_row(); ?>
      <?php if ( get_sub_field( 'show_banner_1' ) == 1 ) { ?>

         <?php $banner_url = get_sub_field( 'banner_url' ); ?>
         <?php if ( $banner_url ) { ?>
         <a href="<?php echo $banner_url['url']; ?>" class="banner banner-img cover display-block gradient2 relative"
   style="background-image: url(<?php the_sub_field( 'background_image' ); ?>);" target="<?php echo $banner_url['target']; ?>">
            <div class="shape-red shape-red__big--top-left shape-red--top-left"></div>
            <div class="shape-grey shape-grey__big--top-left  shape-grey--top-left"></div>
            <div class="container container--indent section">
               <div class="columns">
                  <div class="column is-6">
                     <div class="banner-img__content">
                        <h6 class="banner-img__top-label"><?php the_sub_field( 'tagline' ); ?></h6>
                        <div class="banner-img__content-inner">
                           <h2><strong><?php the_sub_field( 'title' ); ?></strong></h2>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
         <?php } ?>
      <?php } ?>
   <?php endwhile; ?>
<?php endif; ?>

<!-- Banner 2 -->
<?php if ( have_rows( 'promotion_banner_with_blocks_and_external_links' ) ) : ?>
   <?php while ( have_rows( 'promotion_banner_with_blocks_and_external_links' ) ) : the_row(); ?>
      <?php if ( get_sub_field( 'show_banner_2' ) == 1 ) { ?>
         <div class="background-blue banner-training relative">
            <div class="shape-red shape-red__big--top-left shape-red--top-left"></div>
            <div class="shape-grey shape-grey__big--top-left shape-grey--top-left"></div>
            <div class="container container--indent section">
               <div class="columns is-centered is-multiline">
                  <div class="column is-10 padding-bottom-0">
                     <h2 class="section-title"><?php the_sub_field( 'banner_title' ); ?></h2>
                  </div>
                  <?php if ( have_rows( 'banner_blocks' ) ) : ?>
                     <?php while ( have_rows( 'banner_blocks' ) ) : the_row(); ?>
                        <div class="column is-5">
                           <div class="banner-training__content">
                              <h6><?php the_sub_field( 'title' ); ?></h6>
                              <p><?php the_sub_field( 'description' ); ?></p>
                           </div>
                        </div>
                     <?php endwhile; ?>
                  <?php endif; ?>
                  <div class="column is-12 center">
                     <a href="<?php the_sub_field( 'button_url' ); ?>" class="btn" target="_blank"><?php the_sub_field( 'button_text' ); ?></a>
                  </div>
               </div>
            </div>
         </div>
      <?php } ?>
   <?php endwhile; ?>
<?php endif; ?>

<!-- Cart world, cart tragac u akciji  -->
<section class="container container--indent section padding-bottom-0">
   <div class="columns">
      <div class="column">
         <!-- Get science cat  -->
         <?php $cat_name = 'Svet' ?>
         <?php $cat_slug = 'svet'; ?>
         <?php $cart_cat_class = "cart-cat--world"; ?>
         <?php include(locate_template('template-parts/cart-cat.php')); ?>
         <!-- End science cat  -->

      </div>
      <div class="column">
         <!-- Get tragac u akciji cat  -->
         <?php $cat_name = 'Tragač u akciji' ?>
         <?php $cat_slug = 'studije'; ?>
         <?php $cart_cat_class = "cart-cat--tragac"; ?>
         <?php include(locate_template('template-parts/cart-cat.php')); ?>
         <!-- End tragac u akciji cat  -->
      </div>
   </div>
</section>



<?php get_footer(); ?>