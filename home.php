<?php /* Template Name: Blog */ get_header(); ?>


   <div class="container">
      <?php
      while ( have_posts() ) : the_post();

         get_template_part( 'template-parts/content', 'page' );

      endwhile; // End of the loop.
      ?>

      <!-- Adding Previous and Next Post Links -->
      <div class="pagination">
         <?php pagination_nav(); ?>
      </div>
      
      <!-- Adding Page Number Pagination  -->
      <nav class="pagination">
         <?php pagination_bar(); ?>
      </nav>
   </div>

  <?php  wp_reset_postdata(); ?>

   <div class="container">
      <h2>Similar blog</h2>

      <?php 
         $do_not_duplicate = $post->ID;
        
         // get category name 
         $categories = get_the_category();
         $category_name = $categories[0]->name;
      
         $args = array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            'post__not_in' => array($do_not_duplicate),
            'orderby' => 'rand',
            'category__in' => wp_get_post_categories( get_queried_object_id() ),
        );

        $the_query = new WP_Query( $args );

        if ($the_query->have_posts()) : ?>

         <div>
      
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
               <?php get_template_part( 'template-parts/content', 'page' ); ?>
            <?php endwhile; ?>
      
         </div>
      
         <?php 
          endif; 
          wp_reset_postdata();
          ?>
          
   </div>
<?php
// get_sidebar();
get_footer();


