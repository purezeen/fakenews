<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<div class="page404 relative">
   <div class="shape-red shape-red--bottom-right"></div>
   <div class="shape-dark shape-dark--bottom-right"></div>
   <div class="container section columns">
      <div class="column is-4">
         <div class="page404__image">
            <img src="<?php echo get_template_directory_uri() ?>/img/page404.png" alt="404">
         </div>
      </div>
      <div class="column is-8">
         <div class="page404__content">
            <h1><strong>Tražena stranica nije pronađena.</strong> </h1>
            <p>U nastavku pogledajte neke od vesti koje vam preporučujemo.</p>
            <a href="/" class="btn btn-dark">NAZAD NA NASLOVNU</a>
         </div>
      </div>
   </div>
</div>

<!-- Get most reading slider  -->
<?php get_template_part( 'template-parts/most', 'reading' ) ?>
<!-- End most reading slider  -->

<?php $hero_post_id = ""; ?>

<section class="background-white">
   <div class="container container--indent section">
      <div class="columns">
         <div class="column">

            <!-- Get politics cat  -->
            <?php $cat_name = 'Politika' ?>
            <?php $cat_slug = 'politika'; ?>
            <?php $cart_cat_class = "cart-cat--politics"; ?>
            <?php $mgb_large = "mgb-large"; ?>

            <?php include(locate_template('template-parts/cart-cat.php')); ?>
            <!-- End politics cat  -->
         </div>
         <div class="column">
            <!-- Get politics cat  -->
            <?php $cat_name = 'Društvo' ?>
            <?php $cat_slug = 'drustvo-i-kultura'; ?>
            <?php $cart_cat_class = "cart-cat--society"; ?>
            <?php $mgb_large = "mgb-large"; ?>
            <?php include(locate_template('template-parts/cart-cat.php')); ?>
            <!-- End politics cat  -->
         </div>
      </div>
      <div class="columns">
         <div class="column">

            <!-- Get politics cat  -->
            <?php $cat_name = 'Ekonomija' ?>
            <?php $cat_slug = 'ekonomija'; ?>
            <?php $cart_cat_class = "cart-cat--economic"; ?>
            <?php $mgb_large = ""; ?>
            <?php include(locate_template('template-parts/cart-cat.php')); ?>
            <!-- End politics cat  -->
         </div>
         <div class="column">

            <!-- Get politics cat  -->
            <?php $cat_name = 'Nauka' ?>
            <?php $cat_slug = 'kvazinauka'; ?>
            <?php $cart_cat_class = "cart-cat--science"; ?>
            <?php $mgb_large = ""; ?>
            <?php include(locate_template('template-parts/cart-cat.php')); ?>
            <!-- End politics cat  -->
         </div>
      </div>
   </div>
</section>

<?php
get_footer();