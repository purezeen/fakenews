<?php get_header(); ?>

<!-- Get breadcrumbs section  -->
<?php get_template_part( 'template-parts/breadcrumbs', 'section' ); ?>
<!-- End breadcrumbs section  -->

<section class="overflow-hidden">
   <div class="container section layout">
      <div class="layout__main">
         <?php
            $currCat = get_category(get_query_var('cat'));
            $cat_name = $currCat->name;
            $cat_id   = get_cat_ID( $cat_name );
         ?>
         <h1 class="page-title"><?php echo $cat_name; ?></h1>
         <?php
         $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
         $temp = $wp_query;
         $wp_query = null;
         $wp_query = new WP_Query();
         $wp_query->query('showposts=7&post_type=post&paged='.$paged.'&cat='.$cat_id);
         while ($wp_query->have_posts()) : $wp_query->the_post();
         ?>

         <?php get_template_part( 'template-parts/content', 'page' ); ?>
         <?php endwhile; ?>
         <?php 
            wp_reset_query();
         ?>

         <!-- Adding Previous and Next Post Links -->
         <div class="pagination">
            <?php pagination_bar(); ?>
         </div>

      </div>

      <sidebar class="layout__sidebar background-grey">
        <?php get_sidebar(); ?>
      </sidebar>
   </div>
</section>

<?php get_footer(); ?>