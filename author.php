<?php get_header(); ?>

<!-- Get breadcrumbs section  -->
<?php get_template_part( 'template-parts/breadcrumbs', 'section' ); ?>
<!-- End breadcrumbs section  -->

<section class="overflow-hidden">
   <div class="container section layout">
      <div class="layout__main">
         <h1 class="page-title"><?php esc_html_e( 'Autor: ', 'gulp_wordpress' );echo get_the_author(); ?></h1>
         <?php
         if ( have_posts() ) :
       		while ( have_posts() ) : the_post();

         get_template_part( 'template-parts/content', 'page' );
         endwhile;
         endif; ?>

         <!-- Adding Previous and Next Post Links -->
         <div class="pagination">
            <?php pagination_bar(); ?>
         </div>

      </div>

      <sidebar class="layout__sidebar background-grey">
        <?php get_sidebar(); ?>
      </sidebar>
   </div>
</section>

<?php get_footer(); ?>