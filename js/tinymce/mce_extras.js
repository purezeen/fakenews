(function() {
    tinymce.PluginManager.add('lwp_1133_tinymce', function(editor, url) {
        editor.on('init', function() {
            editor.formatter.register('lwp-code', {
                inline: 'span', classes: 'fakenews-label'
            });
        });
        editor.addButton('lwp-code', {
            title: 'Label highlighted text',
            icon: 'code',
            onclick: function() {
                tinymce.activeEditor.formatter.toggle('lwp-code');
            },
            onPostRender: function() {
                var ctrl = this;
                editor.on('NodeChange', function(e) {
                    ctrl.active(
                        e.element.className.indexOf('fakenews-label') !== -1
                    );
                });
            }
        });
    });
})();