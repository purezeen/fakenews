(function ($) {



  /*----------  Navigation  ----------*/

  $(document).ready(function () {

    $(".navbar__burger").click(function () {
      $(".navbar").toggleClass("is-active");
      $("body").toggleClass("disable-scroll");
    });

    var menuHasChilder = $('.menu-item-has-children >a');

    menuHasChilder.after("<span class='nav-arrow'></span>");

    if ($(document).width() < 1024) {
      $('.menu-item-has-children >.nav-arrow').on("click", function (e) {
        $(this).parent('.menu-item-has-children').toggleClass('submenu-toggle');
        e.stopPropagation();
        e.preventDefault();
      });
    }
  });

  /*----------  Searchform input field  ----------*/

  document.addEventListener("DOMContentLoaded", function () {

    let navbar = document.querySelector(".navbar__logo");
    let buttonHome = document.querySelector(".searchform__button");
    let searchform = document.querySelector(".searchform");
    let CountButtonHomeClicks = 0;

    buttonHome.addEventListener("click", function (e) {

      CountButtonHomeClicks += 1;
      navbar.classList.add('.is-visible');
      searchform.classList.add('is-active');
      let has_class = searchform.classList.contains('.is-active');

      // Prevent first click 
      if (CountButtonHomeClicks == 1 && !has_class) {
        e.preventDefault();
        document.getElementById("s").focus();
      }

    });

    /*----------  Slick plugin  ----------*/

    $('.full-width').slick({
      dots: false,
      arrows: false,
      centerMode: true,
      infinite: false,
      centerPadding: 'calc((100% - 1280px)/2)',
      slidesToShow: 3,
      initialSlide: 1,
      swipeToSlide: true,
      responsive: [{
          breakpoint: 1360,
          settings: {
            arrows: false,
            centerMode: true,
            infinite: false,
            centerPadding: '6rem',
            slidesToShow: 3,
            initialSlide: 1,
          }
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            infinite: false,
            centerPadding: '20px',
            initialSlide: 0,
            slidesToShow: 1,
          }
        }
      ]
    });

  });



  /*----------  CyrLatConverter  ----------*/



  $(document).ready(function () {

    $('p > iframe').wrap('<div class="responsive-embed-container" />');

    $(".language a").click(function () {
      var lng = $(this).attr("lang");
      $(".language-wrap").attr("lang", lng);
      return false;
    });

    let CyrLat = new CyrLatConverter('body').init({
      onClickCyr: '#cirilica',
      onClickLat: '#latinica',
      cookieDuration: 7,
      permalinkHash: true,
      ignoreClasses: ['ignore'],
      benchmark: true,
      benchmarkEval: "document.getElementById('exctime').innerHTML = '%s%';"
    });

    CyrLat.C2L();

  });

  var DesktopWidth = window.innerWidth;


  if (DesktopWidth >= 768) {
    /*----------  Sidebar dynamic display  ----------*/
    $widget_height_total = 0;
    check_widget_height();

    function check_widget_height() {
      // Get widget height
      $(".layout__sidebar > div:visible").each(function () {
        let $widget_height = $(this).outerHeight();
        $widget_height_total += $widget_height;
      });
    }
    //   // Get main content height
    $main_height_total = 400;
    $(".layout__main > div").each(function () {
      let $main_height = $(this).outerHeight();
      $main_height_total += $main_height;
    });

    if ($widget_height_total > $main_height_total) {
      $(".layout__sidebar > div:last-of-type").hide();
      $widget_height_total = 0;
      check_widget_height();
      if ($widget_height_total > $main_height_total) {
        $(".layout__sidebar > div:nth-child(3)").hide();
        check_widget_height();
        if ($widget_height_total > $main_height_total) {
          $(".layout__sidebar > div:nth-child(2)").hide();
        }
      }
    }
  }

})(jQuery);