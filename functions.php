<?php
/**
 * gulp-wordpress functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gulp-wordpress
 */

if ( ! function_exists( 'gulp_wordpress_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gulp_wordpress_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on gulp-wordpress, use a find and replace
	 * to change 'gulp-wordpress' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'gulp-wordpress', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	// require_once('navwalker.php');

	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'menuname' ),
		'Footer1' => esc_html__( 'Footer1', 'menuname' ),
		'Footer2' => esc_html__( 'Footer2', 'menuname' ),
		'Footer3' => esc_html__( 'Footer3', 'menuname' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'gulp_wordpress_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'gulp_wordpress_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gulp_wordpress_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gulp_wordpress_content_width', 640 );
}
add_action( 'after_setup_theme', 'gulp_wordpress_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gulp_wordpress_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gulp-wordpress' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gulp-wordpress' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gulp_wordpress_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gulp_wordpress_scripts() {


	wp_enqueue_style( 'bulma-css',  get_template_directory_uri() . '/bulma.css' );

	wp_enqueue_style( 'gulp-wordpress-style', get_stylesheet_uri() );

	wp_enqueue_style( 'slick-css',  get_template_directory_uri() . '/css/slick.css' );

	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '1', true );
	
	wp_enqueue_script( 'cyrlatconverter', get_template_directory_uri() . '/js/cyrlatconverter.min.js', array('jquery'), '1.5', true );

	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gulp_wordpress_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


// Register Custom Post Type
function cpt_CustomPost() {

	$labels = array(
		'name'                  => _x( 'Custom post', 'Job General Name', 'gulp_wordpress' ),
		'singular_name'         => _x( 'Custom post', 'Job Singular Name', 'gulp_wordpress' ),
		'menu_name'             => __( 'Custom post', 'gulp_wordpress' ),
		'name_admin_bar'        => __( 'Custom post', 'gulp_wordpress' ),
		'archives'              => __( 'Item Archives', 'gulp_wordpress' ),
		'attributes'            => __( 'Item Attributes', 'gulp_wordpress' ),
		'parent_item_colon'     => __( 'Parent Item:', 'gulp_wordpress' ),
		'all_items'             => __( 'All Items', 'gulp_wordpress' ),
		'add_new_item'          => __( 'Add New Item', 'gulp_wordpress' ),
		'add_new'               => __( 'Add New', 'gulp_wordpress' ),
		'new_item'              => __( 'New Item', 'gulp_wordpress' ),
		'edit_item'             => __( 'Edit Item', 'gulp_wordpress' ),
		'update_item'           => __( 'Update Item', 'gulp_wordpress' ),
		'view_item'             => __( 'View Item', 'gulp_wordpress' ),
		'view_items'            => __( 'View Items', 'gulp_wordpress' ),
		'search_items'          => __( 'Search Item', 'gulp_wordpress' ),
		'not_found'             => __( 'Not found', 'gulp_wordpress' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'gulp_wordpress' ),
		'featured_image'        => __( 'Featured Image', 'gulp_wordpress' ),
		'set_featured_image'    => __( 'Set featured image', 'gulp_wordpress' ),
		'remove_featured_image' => __( 'Remove featured image', 'gulp_wordpress' ),
		'use_featured_image'    => __( 'Use as featured image', 'gulp_wordpress' ),
		'insert_into_item'      => __( 'Insert into item', 'gulp_wordpress' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'gulp_wordpress' ),
		'items_list'            => __( 'Items list', 'gulp_wordpress' ),
		'items_list_navigation' => __( 'Items list navigation', 'gulp_wordpress' ),
		'filter_items_list'     => __( 'Filter items list', 'gulp_wordpress' ),
	);
	$args = array(
		'label'                 => __( 'Custom post', 'gulp_wordpress' ),
		'description'           => __( 'Custom post Description', 'gulp_wordpress' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'main titile' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'   => 'dashicons-format-image',
		'taxonomies' 	      => array('post_tag'),
	);

	register_post_type( 'custompost', $args );

}

// add_action( 'init', 'cpt_CustomPost', 0 );
 
// add_action( 'init', 'cpt_CustomPost_taxonomy', 0 );

function cpt_CustomPost_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Types', 'taxonomy general name' ),
    'singular_name' => _x( 'Type', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Types' ),
    'all_items' => __( 'All Types' ),
    'parent_item' => __( 'Parent Type' ),
    'parent_item_colon' => __( 'Parent Type:' ),
    'edit_item' => __( 'Edit Type' ), 
    'update_item' => __( 'Update Type' ),
    'add_new_item' => __( 'Add New Type' ),
    'new_item_name' => __( 'New Type Name' ),
    'menu_name' => __( 'Types' ),
  ); 	
 
  register_taxonomy('types',array('custompost'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type' ),
  ));
}


//* ======== EXCERPT ======== *//
function wpdocs_custom_excerpt_length( $length ) {
	if ( !is_category('ukratko') ) {
		 return 20;
	} else {
		 return 40;
	}
}

add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
	// return '...<a href="%1$s" class="link">Više</a>';
	return sprintf( '...<a href="%1$s" class="link">%2$s</a>',
          esc_url( get_permalink( get_the_ID() ) ),
          sprintf( __( 'Više %s', 'wpdocs' ), '' )
    );
}
add_filter('excerpt_more', 'new_excerpt_more');


//* =========== ACF ============ */

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options acf',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
		
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme contact Settings',
		'menu_title'	=> 'Contact',
		'parent_slug'	=> 'theme-general-settings',
	));
}


// Adding Page Number Pagination 
function search_pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
	
    if ($total_pages > 1) {
        $current_page = max(1, get_query_var('paged'));
        echo '<div class="pagination">';
 				if ( $current_page == 1) echo '<a class="prev page-numbers disabled">Prethodna</a>';
        echo paginate_links(array(
	        'base' => '%_%',
	        'format' => '?paged=%#%',
	        'current' => $current_page,
					'total' => $total_pages,
					'prev_next' => true,
					'end_size' => 2,
					'prev_text' => __('Prethodna'),
					'next_text' => __('Sledeća'),
        ));
        if ( $current_page == $wp_query->max_num_pages ) echo '<a class="next page-numbers disabled">Sledeća</a>';
        echo "</div>";
    		echo '<style>.page-numbers.disabled{opacity:.2;}.page-numbers.disabled:hover{color:#000;}</style>'; 
    }
}

function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
	
    if ($total_pages > 1) {
        $current_page = max(1, get_query_var('paged'));
        	$big = 999999999; // need an unlikely integer
 				if ( $current_page == 1) echo '<a class="prev page-numbers disabled">Prethodna</a>';
        echo paginate_links(array(
         	'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
  				'format'  => '?paged=%#%',
	        	'current' => $current_page,
				'total' => $total_pages,
				'prev_next' => true,
				'end_size' => 1,
				'mid_size' => 1,
				'prev_text' => __('Prethodna'),
				'next_text' => __('Sledeća'),
        ));
        if ( $current_page == $wp_query->max_num_pages ) echo '<a class="next page-numbers disabled">Sledeća</a>';
    }
    echo '<style>.page-numbers.disabled{opacity:.2;}.page-numbers.disabled:hover{color:#000;}</style>'; 
}

add_action('pre_get_posts', 'ci_paging_request');
function ci_paging_request($wp)
{
	//We don't want to mess with the admin panel.
	if(is_admin()) return;

	$num = get_option('posts_per_page', 15);

	if( is_category('medjutim') )
		$num = 6; 
	elseif( is_category() )
		$num = 7; 
	// elseif( is_search() )
		// $num = 7; 

	if( ! isset( $wp->query_vars['posts_per_page'] ) and $wp->is_main_query() )
	{
		$wp->query_vars['posts_per_page'] = $num;
	}
}

function searchfilter($query) {
 		$search_term = get_search_query();
 		$latin_sanitize = cyr_to_latin($search_term);
 		// echo 
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('post','page'));
        $query->set('s',$latin_sanitize);
    }
 
return $query;
}
add_filter('pre_get_posts','searchfilter');

// ACF Custom Blocks **************************************************************************************
add_action('acf/init', 'register_acf_block_types');
function register_acf_block_types() {

	if( function_exists('acf_register_block_type') ) {
   // register a testimonial block.
   acf_register_block_type(array(
      'name'              => 'box-link',
      'title'             => __('Box link'),
      'description'       => __('A custom box-link block.'),
      'render_template'   => 'template-parts/block/box-link/box-link.php',
      'category'          => 'formatting',
      'icon'              => 'admin-comments',
      'keywords'          => array( 'testimonial', 'quote' ),
   ));
 }
}



/**
 * Theme Setup 
 *
 */
function ea_setup() {
	// Disable Custom Colors
	add_theme_support( 'disable-custom-colors' );
  
	// Editor Color Palette
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'Red', 'ea-starter' ),
			'slug'  => 'red',
			'color'	=> '#E40C28;',
		),
		array(
			'name'  => __( 'blue', 'ea-starter' ),
			'slug'  => 'blue',
			'color' => '#28559A',
		),
		array(
			'name'  => __( 'black', 'ea-starter' ),
			'slug'  => 'black',
			'color' => 'rgba(21, 21, 22, .8)',
		)
	) );
}
add_action( 'after_setup_theme', 'ea_setup' );


/*
 * Set post views count using post meta
 */
function setPostViews($postID) {
	$countKey = 'post_views_count';
	$count = get_post_meta($postID, $countKey, true);
	if($count==''){
		 $count = 0;
		 delete_post_meta($postID, $countKey);
		 add_post_meta($postID, $countKey, '0');
	}else{
		 $count++;
		 update_post_meta($postID, $countKey, $count);
	}
}



add_filter('deprecated_constructor_trigger_error', '__return_false');


/*
 * Create City tag for Posts
 */
add_action( 'init', 'create_city_post_tag' );
function create_city_post_tag() {
    register_taxonomy( 
            'city_tag', //your tags taxonomy
            'post',  // Your post type
            array( 
                'hierarchical'  => false, 
                'label'         => __( 'City Tags', 'ea-starter' ), 
                'singular_name' => __( 'City Tag', 'ea-starter' ), 
                'rewrite'       => true, 
                'query_var'     => true,
                'show_in_rest'  => true, // Needed for tax to appear in Gutenberg editor.
            )  
        );
}


function lwp_1133_tinymce_plugin( $plugin_array ) {
    $plugin_array['lwp_1133_tinymce'] =
        get_stylesheet_directory_uri() . '/js/tinymce/mce_extras.js';
    return $plugin_array;
}
function lwp_1133_tinymce_buttons( $buttons ) {
    $buttons[] = 'lwp-code';
    return $buttons;
}
function lwp_1133_tinymce() {
    $screen = get_current_screen();
    if ( $screen->base !== 'post' ) return;
    add_filter( 'mce_external_plugins', 'lwp_1133_tinymce_plugin' );
    add_filter( 'mce_buttons', 'lwp_1133_tinymce_buttons' );
}
add_action( 'current_screen', 'lwp_1133_tinymce' );


function re_rewrite_rules() {
    global $wp_rewrite;
    $wp_rewrite->pagination_base    = 'stranica';
}
add_action('init', 're_rewrite_rules');

const CYR = array('а','б','в','г','д','ђ','е','ж','з','и','ј','к','л','љ','м','н','њ','о','п','р','с','т','ћ','у','ф','х','ц','ч','џ','ш','А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','Љ','М','Н','Њ','О','П','Р','С','Т','Ћ','У','Ф','Х','Ц','Ч','Џ','Ш');

const LAT = array('a','b','v','g','d','đ','e','ž','z','i','j','k','l','lj','m','n','nj','o','p','r','s','t','ć','u','f','h','c','č','dž','š','A','B','V','G','D','Đ','E','Ž','Z','I','J','K','L','LJ','M','N','NJ','O','P','R','S','T','Ć','U','F','H','C','Č','DŽ','Š');

function cyr_to_latin( $string ) {
    return str_replace(CYR, LAT, $string);
}

function latin_to_cyr( $string ) {
    return str_replace(LAT, CYR, $string);
}
