<a target="_blank" href="<?php the_field( 'url' ); ?>" class="single-box cover" style="background-image: url(<?php the_field( 'background' ); ?>);">
   <div class="single-box__content overlay">
 

      <h5>
      <?php 
      $content = get_field( 'text' );
      $trimmed_content = substr($content, 0, 90);
      echo $trimmed_content . "...<span class='link'>više</span>";
      ?> 
      </h5>
   </div>
</a>

