<!-- Most reading posts  -->
<section class="background-grey blog-list section">
   <div class="full-width-wrap ">
      <h3 class="section-title blog_border">Najčitanije</h3>

      <div class="full-width">
         <?php
            $args = array(
               'meta_key' => 'post_views_count',
               'post_type' => 'post',
               'category_name' => 'blog, drustvo-i-kultura, ekonomija, kvazinauka, politika, svet, studije',
               'posts_per_page' => 10,
               'post_status' => 'publish',
               'orderby' => 'meta_value_num',
               'order' => 'DESC'
            );
            ?>
            <?php   $the_query = new WP_Query( $args ); ?>

            <?php  if ($the_query->have_posts()) : ?>
               <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                  <?php get_template_part( 'template-parts/post-slider' ) ?>
                  
               <?php endwhile; ?>
            <?php  endif; ?>
      </div>
   </div>
</section>