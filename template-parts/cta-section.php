<!-- Cta  -->
<?php if ( have_rows( 'cta', 'option' ) ) : ?>
   <section class="container container--indent section">
      <article class="cta gradient">
         <div class="shape-red"></div>

         <?php while ( have_rows( 'cta', 'option' ) ) : the_row(); ?>
            <?php $cta_image = get_sub_field( 'cta_image' ); ?>
            <?php if ( $cta_image ) { ?>
               <div class="cta__img">
                  <img src="<?php echo $cta_image['url']; ?>" alt="<?php echo $cta_image['alt']; ?>" />
               </div>
            <?php } ?>
            <div class="cta__content">
               <?php the_sub_field( 'cta_text' ); ?>
            </div>
            <?php $cta_button = get_sub_field( 'cta_button' ); ?>
            <?php if ( $cta_button ) { ?>
               <div class="cta__button">
                  <a class="btn" href="<?php echo $cta_button['url']; ?>" target="<?php echo $cta_button['target']; ?>"><?php echo $cta_button['title']; ?></a>
               </div>
            <?php } ?>
         <?php endwhile; ?>

      </article>
   </section>
<?php endif; ?>

