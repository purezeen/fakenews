<!-- Cart category  -->
<div class="cart-cat <?php echo $cart_cat_class . " " . $mgb_large; ?>">
   <!-- cart-cat--equal-height -->

   <?php
   $args = array(
      'post_type' => 'post',
      'category_name' => $cat_slug,
      'posts_per_page' => 1,
      'post_status' => 'publish',
      'post__not_in' => $hero_post_id
   );
   ?>
   <?php $the_query = new WP_Query( $args ); ?>

   <?php  if ($the_query->have_posts()) : ?>
   <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

   <div>
      <h2 class="section-title"><a href="<?php echo home_url() . "/category/" . $cat_slug ?>"><?php echo $cat_name; ?></a></h2>
      <a href="<?php echo get_permalink(); ?>" class="cart-cat__emphasize relative display-block">
         <div class="fakenews-sticker">
            <h5 class="CyrLatIgnore">FAKE NEWS</h5>
            <?php 
            $tags = get_the_tags();
            $first_tag = (isset($tags[0]) && !empty($tags[0])) ? $tags[0] : '';

            if (is_array($tags)) {
               $tag_count = count($tags);
            } else {
               $tag_count = 0;
            }

            if ( $tag_count >= 1 ) {
               $get_first_tag = "Više medija";
            } else {
               $get_first_tag = (isset($first_tag->name) && !empty($first_tag->name)) ? $first_tag->name : '';
            }
            
             ?>
            <?php if ($get_first_tag): ?>
               <h6><?php echo $get_first_tag; ?></h6>
            <?php endif ?>
         </div>
         <?php 
            if (has_post_thumbnail()) {
               $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'large');
               $backgroundImg = $backgroundImg[0];
            } else {
               $backgroundImg = "";
            }
         ?>

         <div class="cart-cat__emphasize-img image-16-9 cover"
            style="background-image: url(<?php echo $backgroundImg; ?>">
         </div>
         <div class="cart-cat__emphasize-title">
            <h3><?php the_title(); ?></h3>
         </div>
      </a>
   </div>
   <?php endwhile; ?>
   <?php wp_reset_postdata(); ?>
   <?php  endif; ?>

   <?php
   $args = array(
      'post_type' => 'post',
      'category_name' => $cat_slug,
      'posts_per_page' => 3,
      'post_status' => 'publish',
      'post__not_in' => $hero_post_id,
      'offset' => 1
   );
   ?>
   <?php $the_query = new WP_Query( $args ); ?>

   <?php  if ($the_query->have_posts()) : ?>
   <ul class="cart-cat__list list-square list-square--style">

      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

      <li><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>

      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
   </ul>

   <?php  endif; ?>
</div>