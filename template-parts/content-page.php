<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

?>

<div id="post-<?php the_ID(); ?>" class="post-box post-box-categories shadow">	

		<?php $categories = get_the_category();   
			$cat_slug = array();
			// print_r(expression)
			foreach($categories as $cat) {
					$cat_slug[] = $cat->slug;
			}
			
		if (has_post_thumbnail()) {
			$backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
			$backgroundImg = $backgroundImg[0];
		}elseif(in_array("ukratko", $cat_slug)) {
			$backgroundImg = get_template_directory_uri() . "/img/ukratko.png";
			$alt = "ukratko";
		}elseif(in_array("medjutim", $cat_slug)) {
			$backgroundImg = get_template_directory_uri() . "/img/medjutim.png";
		}else {
			$backgroundImg = "";
		}
	?>
	<a href="<?php the_permalink(); ?>" class="post-box__left post-box__image image-16-9 cover"
		style="background-image: url(<?php echo $backgroundImg; ?>)">
	</a>
	<div class="post-box__right post-box__content post-box__content--title-font">
		<h6 class="data"><?php echo get_the_date('d/m/Y'); ?></h6>
		<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>

		<p><?php echo get_the_excerpt();?></p>
	</div>
</div>

