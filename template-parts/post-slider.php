<!-- Post slider item  -->

<div class="cart">
   <?php $categories = get_the_category(); 	
		$cat_slug = array();
		foreach($categories as $cat) {
			 $cat_slug[] = $cat->slug;
      }
   ?>
   
   <?php 
		if (has_post_thumbnail()) {
			$backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
         $backgroundImg = $backgroundImg[0];
         $alt = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
		}elseif(in_array("ukratko", $cat_slug)) {
         $backgroundImg = get_template_directory_uri() . "/img/ukratko.png";
         $alt = "ukratko";
		}elseif(in_array("medjutim", $cat_slug)) {
			$backgroundImg = get_template_directory_uri() . "/img/medjutim.png";
		}else {
         $backgroundImg = "";
         $alt = "medjutim";
      }
   ?>
   
   <div class="cart__image image-16-9 overlay">
      <a href="<?php the_permalink(); ?>"><img src="<?php echo $backgroundImg; ?>" alt="<?php echo $alt; ?>"></a>
   </div>
   <a href="<?php the_permalink(); ?>" class="cart__content display-block">
      <h3><?php the_title(); ?></h3>
      <!-- <span>  -->
      <?php 
      // echo $user_id = get_the_author_meta( 'nickname' ); 
      ?>
      <!-- </span> -->
   </a>
</div>