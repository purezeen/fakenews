<!-- Breadcrumbs section -->
<section class="breadcrumbs-section container section-side">
   <?php
   if ( function_exists('yoast_breadcrumb') ) {
   yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
   }
   ?>
</section>