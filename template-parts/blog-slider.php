<!-- Blog slider  -->
<section class="background-grey blog-list section">
   <div class="full-width-wrap ">
      <h3 class="section-title blog_border"><a href="<?php echo home_url() . "/category/blog"?>">Blog</a></h3>

      <div class="full-width">
         <?php
            $args = array(
               'post_type' => 'post',
               'posts_per_page' => 10,
               'post_status' => 'publish',
               'category_name' => 'blog',
               'order' => 'DESC',
               'post__not_in' => $hero_post_id

            );
            ?>
            <?php   $the_query = new WP_Query( $args ); ?>

            <?php  if ($the_query->have_posts()) : ?>
               <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                  <?php get_template_part( 'template-parts/post-slider' ) ?>
                  
               <?php endwhile; ?>
            <?php  endif; ?>
      </div>
   </div>
</section>