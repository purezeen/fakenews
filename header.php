<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
   <meta charset="<?php bloginfo( 'charset' ); ?>">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="profile" href="http://gmpg.org/xfn/11">
   <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

   <!-- Google fonts  -->
   <link rel="preconnect" href="https://fonts.gstatic.com">
   <link
      href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300;400;700&family=Montserrat:wght@300;400;500;600;700;800&display=swap"
      rel="stylesheet">

   <?php wp_head(); ?>

</head>


<body <?php body_class(); ?>>
   <div id="page" class="site">
   
      <nav class="navbar container">
         <div class="navbar__brand">
            <a href="/" class="navbar__logo">
               <img src="<?php echo get_template_directory_uri()?>/img/fakenews-logo.svg">
            </a>

            <a role="button" class="navbar__burger">
               <span></span>
               <span></span>
               <span></span>
            </a>
         </div>

         <div class="navbar__list-wrap">
            <?php
               wp_nav_menu( array(
                  'theme_location'    => 'primary',
                  'depth'             => 4,
                  'container'         => false,
                  'menu_class'        => 'navbar__list unstyle-list'
                  )
               );
               ?>
                 <a href="/prijavi-laznu-vest/" class="btn navbar__btn">PRIJAVI LAŽNU VEST!</a>
            <div class="navbar-right__button">
               <div class="search-input mobile-header">
                  <?php 
                  get_search_form(); 
               ?>
               </div>
               <ul class="language-wrap" lang="lat">
                     <li class="language lang-lat"><a lang="lat" href="#lat" id="latinica">Lat</a></li>
                     <li class="language lang-cir"><a lang="cir" href="#cir" id="cirilica">Ćir</a></li>
                  </ul>

            </div>
         </div>

      </nav>


      <div id="content" class="site-content">