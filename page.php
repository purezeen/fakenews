<?php get_header(); ?>

<?php
   while ( have_posts() ) : the_post();
?>

<section class="overflow-hidden">
   <div class="container single-title section padding-bottom-0">
      <h1><?php the_title(); ?></h1>
   </div>
   <div class="container section layout">
      <div class="layout__main">
       
         <?php 
            if (has_post_thumbnail()) {
               $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'large');
               $backgroundImg = $backgroundImg[0];
               ?>
               <div class="single-image cover image-16-9"
               style="background: linear-gradient(to right, rgba(0,0,0,.6) 0%, rgba(0, 0, 0, 0) 100%) ,url(<?php echo $backgroundImg; ?>) no-repeat center; background-size: cover;">
            </div>
            <?php
            }
         ?>
   
         <div class="single-page editor">
            <?php the_content(); ?>
         </div>

         <?php
			endwhile; // End of the loop.
         ?>

      </div>

      <sidebar class="layout__sidebar background-grey">
        <?php get_sidebar(); ?>
      </sidebar>
   </div>
</section>


<section class="background-grey blog-list section">
   <div class="full-width-wrap ">
      <h3 class="section-title blog_border">Najčitanije</h3>

      <div class="full-width">
         <?php
            $args = array(
               'post_type' => 'post',
               'posts_per_page' => 10,
               'post_status' => 'publish',
               'order' => 'DESC'
            );
            ?>
            <?php   $the_query = new WP_Query( $args ); ?>

            <?php  if ($the_query->have_posts()) : ?>
               <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                  <?php get_template_part( 'template-parts/post-slider' ) ?>
                  
               <?php endwhile; ?>
            <?php  endif; ?>
      </div>
   </div>
</section>

<?php
get_footer();