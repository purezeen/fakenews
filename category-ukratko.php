<?php /* Template Name: Ukratko */ get_header(); ?>
<section class="breadcrumbs-section container section-side">
   <?php
   if ( function_exists('yoast_breadcrumb') ) {
   yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
   }
   ?>
</section>

<?php
   $currCat = get_category(get_query_var('cat'));
   $cat_name = $currCat->name;
   $cat_id   = get_cat_ID( $cat_name );
?>


<!-- Page title  -->
<section class="page-section-title background-dark">
   <div class="container columns section-side">
      <div class="column is-3 padding-bottom-0">
         <h1>Ukratko</h1>
      </div>
      <div class="column is-9">
      <p><?php echo $currCat->category_description; ?></p>
      </div>
   </div>
</section>

<section class="overflow-hidden">
   <div class="container section layout">
      <div class="layout__main">

         <?php
         $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
         $temp = $wp_query;
         $wp_query = null;
         $wp_query = new WP_Query();
         $wp_query->query('showposts=7&post_type=post&paged='.$paged.'&cat='.$cat_id);
         while ($wp_query->have_posts()) : $wp_query->the_post();
         ?>
        
         <div class="post-box shadow post-box--border-left">
            <div class="post-box__right post-box__content">
               <h6 class="data"><?php echo get_the_date(); ?></h6>
               <h5><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h5>
               <p><?php echo get_the_excerpt();?></p>
            </div>
         </div>
         <?php endwhile; ?>

         <?php 
            wp_reset_query();
         ?>
         <!-- Adding Previous and Next Post Links -->
         <div class="pagination">
            <?php pagination_bar(); ?>
         </div>


      </div>
   
      <sidebar class="layout__sidebar background-grey">
        <?php get_sidebar(); ?>
      </sidebar>
   </div>
</section>

<!-- Get most reading slider  -->
<?php get_template_part( 'template-parts/most', 'reading' ) ?>
<!-- End most reading slider  -->


<?php get_footer(); ?>