<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>

</div><!-- #content -->

<!-- Get cta  -->
<?php get_template_part( 'template-parts/cta', 'section' ) ?>
<!-- End cta  -->
<!-- <div id="exctime" style="display: none;"></div> -->
<footer class="footer background-grey relative">
   <div class="shape-red shape-red--top-right"></div>  
   <div class="container section padding-bottom-0">
      <div class="footer-wrap">
         <div class="footer__column footer_1">

            <a href="/" class="footer__logo">
               <img src="<?php echo get_template_directory_uri() ?>/img/fakenews-logo.svg" alt="fakenews">
            </a>

            <a href="https://novinarska-skola.org.rs" class="footer__logo" target="_blank">
               <img src="<?php echo get_template_directory_uri() ?>/img/NNS.png" alt="fakenews">
            </a>
         </div>
        
         <?php
         wp_nav_menu( array(
            'theme_location'    => 'Footer1',
            'depth'             => 2,
            'container'         => false,
            'menu_class'        => 'unstyle-list footer__column footer_2'
            )
         );
         ?>

         <?php
         wp_nav_menu( array(
            'theme_location'    => 'Footer2',
            'depth'             => 2,
            'container'         => false,
            'menu_class'        => 'unstyle-list footer__column footer_3'
            )
         );
         ?>

         <?php
         wp_nav_menu( array(
            'theme_location'    => 'Footer3',
            'depth'             => 2,
            'container'         => false,
            'menu_class'        => 'unstyle-list footer__column footer_4'
            )
         );
         ?>

         <div class="footer_5 footer__column">
            <h6 class="footer-main-title">Pratite nas</h6>
            <div class="social-icon">
               <a href="https://www.facebook.com/FNTragac/" target="_blank"><img src="<?php echo get_template_directory_uri()?>/img/facebook-icon.png" alt="facebook"></a>
               <a href="https://twitter.com/FNTragac" target="_blank"><img src="<?php echo get_template_directory_uri()?>/img/twitter-icon.png" alt="facebook"></a>
               <a href="https://www.youtube.com/channel/UCJeIM8Te63d6npVYSFerZbQ" target="_blank"><img src="<?php echo get_template_directory_uri()?>/img/youtube-icon.png" alt="facebook"></a>
            </div>
            <a href="/prijavi-laznu-vest/" class="btn">Prijavi lažnu vest!</a>
         </div>
      </div>
      <div class="footer-bottom">
         <div class="footer-bottom-menu">
            <a href="<?php home_url() ?>/impresum">IMPRESUM</a><a href="<?php home_url() ?>/o-nama">O nama</a>
         </div>
         <div class="footer-copyright">
            <p>© 2020 Fake News tragač - All rights reserved. Design by <strong><a href="https://www.weareelder.com/" target="_blank"> Elder</a></strong></p>
         </div>
      </div>
   </div>
</footer>
</div><!-- #page -->

<?php wp_footer(); ?>


</body>

</html>