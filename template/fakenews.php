<?php /* Template Name: Fakenews */ get_header(); ?>

<section class="breadcrumbs-section container section-side">
   <?php
   if ( function_exists('yoast_breadcrumb') ) {
   yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
   }
   ?>
</section>

<section class="fakenews-header relative background-pattern">
   <div class="shape-red shape-red--top-left"></div>
   <div class="shape-grey shape-grey--top-left"></div>
   <div class="shape-red shape-red--top-right"></div>
   <div class="container container--indent section">
      <div class="columns">
         <div class="column is-10">
            <div>
               <h1><strong>Sumnjate u vest koju ste procitali?</strong><br>Pošaljite nam je. Proverićemo.<h1>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="container container--indent section-side fakenews-steps-section">
   <div class="fakenews-steps">
      <div class="fakenews-step">
         <div class="fakenews-step__sticker">
            <div class="fakenews-step__sticker--background">
            </div>
            <h5>Korak</h5>
            <h6>1</h6>
         </div>
         <div class="fakenews-step__background">
            <img src="<?php echo get_template_directory_uri() ?>/img/background1.png)">
         </div>
         <div class="fakenews-step__content">
            <div class="fakenews-step__img">
               <img src="<?php echo get_template_directory_uri() ?>/img/step1.png" alt="korak1">
            </div>
            <p>Uočili ste vest koja vam deluje sumnjivo</p>
         </div>
      </div>
      <div class="fakenews-step">
         <div class="fakenews-step__sticker">
            <div class="fakenews-step__sticker--background">
            </div>
            <h5>Korak</h5>
            <h6>2</h6>
         </div>
         <div class="fakenews-step__background">
            <img src="<?php echo get_template_directory_uri() ?>/img/background2.png)">
         </div>
         <div class="fakenews-step__img">
            <img src="<?php echo get_template_directory_uri() ?>/img/step2.svg" alt="korak2">
         </div>
         <p>U samo četiri klika prijavite vest putem formulara na sajtu fakenews.rs</p>
      </div>
      <div class="fakenews-step">
         <div class="fakenews-step__sticker">
            <div class="fakenews-step__sticker--background">
            </div>
            <h5>Korak</h5>
            <h6>3</h6>
         </div>
         <div class="fakenews-step__background">
            <img src="<?php echo get_template_directory_uri() ?>/img/background3.png)">
         </div>
         <div class="fakenews-step__content">
            <div class="fakenews-step__img">
               <img src="<?php echo get_template_directory_uri() ?>/img/step3.svg" alt="korak3">
            </div>
            <p>Naš tim će u najkraćem mogućem roku ispitati da li je reč o dezinformaciji</p>
         </div>
      </div>
      <div class="fakenews-step">
         <div class="fakenews-step__sticker">
            <div class="fakenews-step__sticker--background">
            </div>
            <h5>Korak</h5>
            <h6>4</h6>
         </div>
         <div class="fakenews-step__background">
            <img src="<?php echo get_template_directory_uri() ?>/img/background4.png)">
         </div>
         <div class="fakenews-step__content">
            <div class="fakenews-step__img">
               <img src="<?php echo get_template_directory_uri() ?>/img/step4.svg" alt="korak4">
            </div>
            <p>Ako je vest lažna, objavićemo tekst o njoj. Na taj način se zajednički borimo protiv dezinformacija</p>
         </div>
      </div>
   </div>
</section>

<section class="container container--indent section editor">
   <div class="columns">
      <div class="column">
         <?php the_content(); ?>
      </div>
   </div>
</section>
<?php get_footer(); ?>