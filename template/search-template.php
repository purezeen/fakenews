<?php /* Template Name: Pretraga */ get_header(); ?>

<section class="background-dark">
   <div class="container container--indent section">
      <form role="search" method="get" class="searchform-large" action="<?php echo esc_url( home_url( '/' ) ); ?>">
         <label class="searchform-large__wrap" for="s">
            <input class="searchform-large__input" type="text" value="<?php echo get_search_query(); ?>" name="s"
               id="s" />

            <input class="searchform-large__button" type="submit" id="searchsubmit" value="" />
         </label>
      </form><i class="fa fa-search"></i>
   </div>
</section>
<section class="overflow-hidden">
   <div class="container section container--indent">

      <a href="" class="post-box shadow post-box__big-img">
         <div class="relative post-box__left">
            <div class="post-box__image image-16-9 cover"
               style="background-image: url(<?php echo get_template_directory_uri()?>/img/Bojkot.png);">
            </div>
            <div class="fakenews-sticker">
               <h5 class="CyrLatIgnore">FAKE NEWS</h5>
               <span href="#">
                  <h6>Alo</h6>
               </span>
            </div>
         </div>

         <div class="post-box__right post-box__content post-box__content--title-font">
            <h5>SNS-ov spot zabranio je REM, a ne opozicija niti N1</h5>

            <p>Na jučerašnjoj naslovnoj strani Informera navodi se da je „zabranjen Vučićev spot”, nastao u okviru
               predizborne kampanje Srpske napredne stranke “Za našu decu”, u kojem se predsednik… Više</p>
         </div>
      </a>

      <a href="" class="post-box shadow post-box__big-img">
         <div class="relative post-box__left">
            <div class="post-box__image image-16-9 cover"
               style="background-image: url(<?php echo get_template_directory_uri()?>/img/Bojkot.png);">
            </div>
            <div class="fakenews-sticker">
               <h5 class="CyrLatIgnore">FAKE NEWS</h5>
               <span href="#">
                  <h6>Alo</h6>
               </span>
            </div>
         </div>

         <div class="post-box__right post-box__content post-box__content--title-font">
            <h5>SNS-ov spot zabranio je REM, a ne opozicija niti N1</h5>

            <p>Na jučerašnjoj naslovnoj strani Informera navodi se da je „zabranjen Vučićev spot”, nastao u okviru
               predizborne kampanje Srpske napredne stranke “Za našu decu”, u kojem se predsednik… Više</p>
         </div>
      </a>

      <a href="" class="post-box shadow post-box__big-img">
         <div class="relative post-box__left">
            <div class="post-box__image image-16-9 cover"
               style="background-image: url(<?php echo get_template_directory_uri()?>/img/Bojkot.png);">
            </div>
            <div class="fakenews-sticker">
               <h5 class="CyrLatIgnore">FAKE NEWS</h5>
               <span href="#">
                  <h6>Alo</h6>
               </span>
            </div>
         </div>

         <div class="post-box__right post-box__content post-box__content--title-font">
            <h5>SNS-ov spot zabranio je REM, a ne opozicija niti N1</h5>

            <p>Na jučerašnjoj naslovnoj strani Informera navodi se da je „zabranjen Vučićev spot”, nastao u okviru
               predizborne kampanje Srpske napredne stranke “Za našu decu”, u kojem se predsednik… Više</p>
         </div>
      </a>
   </div>

</section>

<?php get_footer(); ?>