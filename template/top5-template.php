
<?php /* Template Name: top5 */ get_header(); ?>

<section class="container container--indent section">
   <div class="columns is-centered">
   <div class="column is-6">
      <?php 
           $top_5_tags = get_terms(array(
                'taxonomy' => 'city_tag',
                'orderby' => 'count',
                'order' => 'DESC',
                'number' => '5'
            ));
           if ($top_5_tags) { ?>
            <div class="top-list shadow">
               <div class="top-list__title background-blue">
                  <h3 class="section-title">TOP 5 – NAJCEŠĆE LOKACIJE MANIPULACIJA</h3>
               </div>
                  <ul class="top-list__name top-list__name--first top-list--blue unstyle-list">
                  <?php
                  foreach ($top_5_tags as $top_tags) {
                     $home_url = home_url() . '/city_tag/' . $top_tags->slug;
                     echo '<li><a href="' . $home_url . '">' . $top_tags->name . '</a></li>';
                  }
                  ?>
                  </ul>
            </div>
         <?php
         } 
         ?>
   </div>
   </div>
</section>

<!-- Get most reading slider  -->
<?php get_template_part( 'template-parts/most', 'reading' ) ?>
<!-- End most reading slider  -->

<?php $hero_post_id = ""; ?>

<section class="background-white">
   <div class="container container--indent section">
      <div class="columns">
         <div class="column">

            <!-- Get politics cat  -->
            <?php $cat_name = 'Politika' ?>
            <?php $cat_slug = 'politika'; ?>
            <?php $cart_cat_class = "cart-cat--politics"; ?>
            <?php $mgb_large = "mgb-large"; ?>
            <?php include(locate_template('template-parts/cart-cat.php')); ?>
            <!-- End politics cat  -->
         </div>
         <div class="column">
            <!-- Get politics cat  -->
            <?php $cat_name = 'Društvo' ?>
            <?php $cat_slug = 'drustvo-i-kultura'; ?>
            <?php $cart_cat_class = "cart-cat--society"; ?>
            <?php $mgb_large = "mgb-large"; ?>
            <?php include(locate_template('template-parts/cart-cat.php')); ?>
            <!-- End politics cat  -->
         </div>
      </div>
      <div class="columns">
         <div class="column">

            <!-- Get politics cat  -->
            <?php $cat_name = 'Ekonomija' ?>
            <?php $cat_slug = 'ekonomija'; ?>
            <?php $cart_cat_class = "cart-cat--economic"; ?>
            <?php $mgb_large = ""; ?>
            <?php include(locate_template('template-parts/cart-cat.php')); ?>
            <!-- End politics cat  -->
         </div>
         <div class="column">

            <!-- Get politics cat  -->
            <?php $cat_name = 'Nauka' ?>
            <?php $cat_slug = 'kvazinauka'; ?>
            <?php $cart_cat_class = "cart-cat--science"; ?>
            <?php $mgb_large = ""; ?>
            <?php include(locate_template('template-parts/cart-cat.php')); ?>
            <!-- End politics cat  -->
         </div>
      </div>
   </div>
</section>


<?php
get_footer();