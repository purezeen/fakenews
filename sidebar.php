<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */
?>
<div class="most-read  mgb-medium">
   <h2 class="section-title brief_border"><a href="#">NAJČITANIJE</a></h2>
   <?php
   $args = array(
      'meta_key' => 'post_views_count',
      'post_type' => 'post',
      'category_name' => 'blog, drustvo-i-kultura, ekonomija, kvazinauka, politika, svet, studije',
      'posts_per_page' => 6,
      'post_status' => 'publish',
      'orderby' => 'meta_value_num',
      'order' => 'DESC'
   );
   ?>
   <?php   $the_query = new WP_Query( $args ); ?>

   <?php  if ($the_query->have_posts()) : ?>
      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
         <?php $categories = get_the_category();   
            $cat_slug = array();
            // print_r(expression)
            foreach($categories as $cat) {
                $cat_slug[] = $cat->slug;
            }
            foreach($cat_slug as $slug) {
               switch ($slug) {
                  case "politika":
                     $list_square_color = "politics-color";
                     break;
                  case "drustvo-i-kultura":
                     $list_square_color = "society-color";
                     break;
                  case "blog":
                     $list_square_color = "blog-color";
                     break;
                     case "ekonomija":
                     $list_square_color = "economy-color";
                     break;
                     case "kvazinauka":
                     $list_square_color = "science-color";
                     break;
                     case "svet":
                     $list_square_color = "world-color";
                     break;
                  case "studije":
                     $list_square_color = "tragac-color";
                     break;
                     case "medjutim":
                        $list_square_color = "truth-color";
                        break;
                              
                  default:
                     $list_square_color = "politics-color";
                  } 
            }
         ?>
         
         <?php 
            if (has_post_thumbnail()) {
               $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
               $backgroundImg = $backgroundImg[0];
               $alt = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
            }elseif(in_array("ukratko", $cat_slug)) {
               $backgroundImg = get_template_directory_uri() . "/img/ukratko.png";
               $alt = "ukratko";
            }elseif(in_array("medjutim", $cat_slug)) {
               $backgroundImg = get_template_directory_uri() . "/img/medjutim.png";
            }else {
               $backgroundImg = "";
               $alt = "medjutim";
            }
         ?>
         <a href="<?php echo get_permalink(); ?>" class="most-read__row">
            <div class="most-read__img cover"
               style="background-image: url(<?php echo $backgroundImg; ?>);">

            </div>
            <div class="most-read__content">
               <h6 class="<?php echo $list_square_color; ?>"><?php echo ($categories[1] == null) ? $categories[0]->name : $categories[1]->name; ?></h6>
               <h5><?php the_title(); ?></h5>
            </div>
         </a>
      <?php endwhile; ?>
   <?php  endif; ?>
</div>

<?php
   $cat_id   = get_cat_ID( 'ukratko' );
?>
<div class="brief shadow mgb-medium">
   <h2 class="section-title brief_border"><a href="<?php home_url() ?>/category/ukratko">Ukratko</a></h2>
   <div class="brief-scroll-slider">

         <?php
         $temp = $wp_query;
         $wp_query = null;
         $wp_query = new WP_Query();
         $wp_query->query('showposts=3&post_type=post&cat='.$cat_id);
         while ($wp_query->have_posts()) : $wp_query->the_post();
         ?>

      <article class="brief-box">
         <h4 class="data"><?php echo get_the_date(); ?></h4>
         <a href="<?php echo get_permalink(); ?>">
            <h3><?php the_title(); ?></h3>
         </a>
         <p><?php echo wp_trim_words(get_the_content(), 27) ?></p>
      </article>

      <?php endwhile; ?>

      <?php 
         wp_reset_query();
      ?>

   </div>
</div>
<?php 
$top_5_tags = get_terms(array(
    'taxonomy' => 'city_tag',
    'orderby' => 'count',
    'order' => 'DESC',
    'number' => '5'
));
if ($top_5_tags) { ?>
<div class="top-list shadow mgb-medium">
   <div class="top-list__title background-blue">
      <h6>TOP 5 – LOKACIJA MANIPULACIJA</h6>
   </div>
   <ul class="top-list__name top-list__name--first top-list--blue unstyle-list">
    <?php
      foreach ($top_5_tags as $top_tags) {
         $home_url = home_url() . '/city_tag/' . $top_tags->slug;
         echo '<li><a href="' . $home_url . '">' . $top_tags->name . '</a></li>';
      }
      ?>
   </ul>
</div>
   <?php
   } 
   ?>

<div class="top-list shadow">
	<div class="top-list__title background-main">
		<h6>TOP 15 - IZVORI MANIPULACIJA</h6>
	</div>
	<ul class="top-list__name top-list__name--second unstyle-list">
		 <?php 
        $top_15_tags = get_terms(array(
             'taxonomy' => 'post_tag',
             'orderby' => 'count',
             'order' => 'DESC',
             'number' => '15'
         ));
         foreach ($top_15_tags as $top_tags) {
            $home_url = home_url() . '/tag/' . $top_tags->slug;
            echo '<li><a href="' . $home_url . '">' . $top_tags->name . '</a></li>';
         }
       ?>
	</ul>
</div>