<?php  get_header(); ?>

<?php $filter['s'] = $_GET['s']; ?>
<section class="background-dark">
   <div class="container container--indent section">
      <form role="search" method="get" class="searchform-large" action="<?php echo esc_url( home_url( '/' ) ); ?>">
         <label class="searchform-large__wrap" for="s">
            <input class="searchform-large__input" type="text" value="<?php echo $filter['s']; ?>" name="s" id="s" />
            <input class="searchform-large__button" type="submit" id="searchsubmit" value="" />
         </label>
      </form><i class="fa fa-search"></i>
   </div>
</section>
<section class="overflow-hidden">
   <div class="container section container--indent">
     <?php
      if ( have_posts() ) :
      while ( have_posts() ) : the_post(); 
			 $categories = get_the_category(); 	
					$cat_slug = array();
					foreach($categories as $cat) {
						 $cat_slug[] = $cat->slug;
			      }
			 
	     if (has_post_thumbnail()) {
				$backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
	         $backgroundImg = $backgroundImg[0];
	         $alt = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
			}elseif(in_array("ukratko", $cat_slug)) {
	         $backgroundImg = get_template_directory_uri() . "/img/ukratko.png";
	         $alt = "ukratko";
			}elseif(in_array("medjutim", $cat_slug)) {
				$backgroundImg = get_template_directory_uri() . "/img/medjutim.png";
			}else {
	         $backgroundImg = "";
	         $alt = "medjutim";
	      }
      ?>
      <div class="post-box shadow post-box__big-img">
         <div class="relative post-box__left">
            <a href="<?php the_permalink(); ?>" class="post-box__image image-16-9 cover"
               style="background-image: url(<?php echo $backgroundImg; ?>);">
            </a>
            <div class="fakenews-sticker">
               <h5 class="CyrLatIgnore">FAKE NEWS</h5>
               <span>
                  <?php 
                  $tags = get_the_tags();
                  $first_tag = (isset($tags[0]) && !empty($tags[0])) ? $tags[0] : '';

                  if (is_array($tags)) {
                     $tag_count = count($tags);
                  } else {
                     $tag_count = 0;
                  }

                  if ( $tag_count > 1 ) {
                     $get_first_tag = "Više medija";
                  } else {
                     $get_first_tag = (isset($first_tag->name) && !empty($first_tag->name)) ? $first_tag->name : '';
                  }
                  
                   ?>
                  <?php if ($get_first_tag): ?>
                     <h6><?php echo $get_first_tag; ?></h6>
                  <?php endif ?>
               </span>
            </div>
         </div>

         <div class="post-box__right post-box__content post-box__content--title-font">
            <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>

            <p><?php echo get_the_excerpt();?></p>
         </div>
      </div>
      <?php
      endwhile;
      else:
         echo "<h1>Ništa nije pronađeno</h1>";
         echo "<p>Žao nam je, ali ništa se ne podudara sa vašim terminima za pretragu. Pokušajte ponovo sa različitim ključnim rečima.</p>";
      endif; ?>

      <!-- Adding Previous and Next Post Links -->
      <?php search_pagination_bar(); ?>

   </div>

</section>

<?php get_footer(); ?>