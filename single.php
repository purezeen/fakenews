<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<!-- Count the number of view -->
<?php setPostViews(get_the_ID()); ?>


<?php
   while ( have_posts() ) : the_post();
  
?>

<section class="overflow-hidden">
   <div class="container single-title section padding-bottom-0">
      <h1><?php the_title(); ?></h1>
   </div>
   <div class="container section layout">
      <div class="layout__main">
         <div class="post-info">
            <!-- <h6 class="author"><strong>Autor:</strong> <?php the_author(); ?></h6> -->
            <h6 class="data"><img class="data-icon" src="<?php echo get_template_directory_uri()?>/img/data-icon.png"
                  alt="data"><?php echo get_the_date("d.m.Y."); ?></h6>
         </div>

         <?php 
            if (has_post_thumbnail()) {
               $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'large');
               $backgroundImg = $backgroundImg[0];
               ?>
         <div class="single-image cover image-16-9" style="background-image: url(<?php echo $backgroundImg; ?>">
         </div>
         <?php
            }
         ?>

         <div class="single-page editor drop-cap">
            <?php the_content(); ?>

         </div>

         <?php
			endwhile; // End of the loop.
         ?>

         <div class="single-page-footer">
            <?php
               $posttags = get_the_tags();
               if ($posttags) {
                  ?>
            <div class="tag-list">
               <h6>Tagovi:</h6>
               <?php
                  foreach($posttags as $tag) {
                     ?>
               <a class="tag" href="<?php echo get_tag_link($tag->term_id); ?>"><?php echo $tag->name; ?></a>
               <?php
                  }
                  $posttax = get_the_terms( get_the_ID(), 'city_tag');

                  if($posttax) {
                  foreach ( $posttax as $tax ) {
                  ?>
                     <a class="tag" href="<?php echo get_tag_link($tax->term_id); ?>"><?php echo  __( $tax->name ); ?></a>
                  <?php
                  }
                  }
                  ?>
            </div>
            <?php
               }
            ?>

            <div class="share-button">
               <h6>Podeli:</h6>
               <?php echo do_shortcode('[Sassy_Social_Share]') ?>
            </div>
         </div>
      </div>

      <sidebar class="layout__sidebar background-grey">
         <?php get_sidebar(); ?>
      </sidebar>
   </div>
</section>


<section class="background-grey blog-list section">
   <div class="full-width-wrap ">
      <h3 class="section-title blog_border">Slični članci</h3>

      <div class="full-width">
         <?php
            $post_categories = wp_get_post_categories( $post->ID );
            $args = array(
               'post_type' => 'post',
               'posts_per_page' => 10,
               'post_status' => 'publish',
               'order' => 'DESC',
               'category__and' =>$post_categories
            );
            ?>
         <?php   $the_query = new WP_Query( $args ); ?>
         <?php  if ($the_query->have_posts()) : ?>
         <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

         <?php get_template_part( 'template-parts/post-slider' ) ?>

         <?php endwhile; ?>
         <?php  endif; ?>
      </div>
   </div>
</section>

<?php
get_footer();